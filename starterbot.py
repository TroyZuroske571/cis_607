import os
import time
from slackclient import SlackClient
from random import randint

randNum = randint(0,100)

# starterbot's ID as an environment variable
BOT_ID = os.environ.get("BOT_ID")

# constants
AT_BOT = "<@" + BOT_ID + ">"
EXAMPLE_COMMAND = "do"

# instantiate Slack & Twilio clients
slack_client = SlackClient(os.environ.get('SLACK_BOT_TOKEN'))

def parse_slack_output(slack_rtm_output):
    """
        The Slack Real Time Messaging API is an events firehose.
        this parsing function returns None unless a message is
        directed at the Bot, based on its ID.
    """
    output_list = slack_rtm_output
    if output_list and len(output_list) > 0:
        for output in output_list:
            if output and 'text' in output and AT_BOT in output['text']:
                # return text after the @ mention, whitespace removed
                return output['text'].split(AT_BOT)[1].strip().lower(), \
                       output['channel']
    return None, None


class State:
    def handle_command(self, command, channel):
        assert 0, "handle command not implemented"


class InitialState(State):
    def __init__(self):
        print('creating InitialState')

    def handle_command(self, command, channel):
        print("InitialState receiving " + command)
        try:
            value = int(command)
        except:
            response = "I don't recognize this as a number: " + command
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return NonNumberState()
        if value < target_number:
            response = 'Too low'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return LowState1()
        if value > target_number:
            response = 'Too high'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return HighState1()
        if value == target_number:
            response = 'Correct'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return None


class NonNumberState(State):
    def __init__(self):
        print('creating NonNumber')

    def handle_command(self, command, channel):
        print("NonNumber receiving " + command)
        try:
            value = int(command)
        except:
            response = "I don't recognize this as a number: " + command
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return NonNumberState()
        if value < target_number:
            response = 'Too low'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return LowState1()
        if value > target_number:
            response = 'Too high'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return HighState1()
        if value == target_number:
            response = 'Correct'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return None


class LowState1(State):
    def __init__(self):
        print('creating LowState1')

    def handle_command(self, command, channel):
        print("LowState1 receiving " + command)
        try:
            value = int(command)
        except:
            response = "I don't recognize this as a number: " + command
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return NonNumberState()
        if value < target_number:
            response = 'Too low'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return LowState2()
        if value > target_number:
            response = 'Too high'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return HighState1()
        if value == target_number:
            response = 'Correct'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return None


class LowState2(State):
    def __init__(self):
        print('creating LowState2')

    def handle_command(self, command, channel):
        print("LowState2 receiving " + command)
        try:
            value = int(command)
        except:
            response = "I don't recognize this as a number: " + command
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return NonNumberState()
        if value < target_number:
            response = 'Too low'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return None
        if value > target_number:
            response = 'Too high'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return HighState1()
        if value == target_number:
            response = 'Correct'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return None


class HighState1(State):
    def __init__(self):
        print('creating HighState1')

    def handle_command(self, command, channel):
        print("HighState1 receiving " + command)
        try:
            value = int(command)
        except:
            response = "I don't recognize this as a number: " + command
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return NonNumberState()
        if value < target_number:
            response = 'Too low'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return LowState1()
        if value > target_number:
            response = 'Too high'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return HighState2()
        if value == target_number:
            response = 'Correct'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return None


class HighState2(State):
    def __init__(self):
        print('creating HighState2')

    def handle_command(self, command, channel):
        print("HighState2 receiving " + command)
        try:
            value = int(command)
        except:
            response = "I don't recognize this as a number: " + command
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return NonNumberState()
        if value < target_number:
            response = 'Too low'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return LowState1()
        if value > target_number:
            response = 'Too high'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return None
        if value == target_number:
            response = 'Correct'
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text=response, as_user=True)
            return None


class StateMachine:
    def __init__(self):
        self.guess_history = []
        self.current_state = InitialState()
        self.READ_WEBSOCKET_DELAY = 1  # 1 second delay

    def run(self):
        if slack_client.rtm_connect():
            print("StarterBot connected and running!")
            while True:
                command, channel = parse_slack_output(slack_client.rtm_read())
                if command and channel:
                    self.guess_history.append(command)
                    self.current_state = self.current_state.handle_command(command, channel)
                    if self.current_state == None:
                        print('Quitting')
                        break
                time.sleep(self.READ_WEBSOCKET_DELAY)
        else:
            print("Connection failed. Invalid Slack token or bot ID?")

if __name__ == "__main__":
    target_number = randint(1, 100)
    print(target_number)
    game = StateMachine()
    game.run()

print('Game has exited.')